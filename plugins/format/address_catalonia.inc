<?php

/**
 * @file
 * A specific handler for CT.
 */

$plugin = array(
  'title' => t('Address form (Catalonia provinces add-on)'),
  'format callback' => 'addressfield_format_address_catalonia_generate',
  'type' => 'address',
  'weight' => -80,
);

function addressfield_format_address_catalonia_generate(&$format, $address, $context = array()) {
  if ($address['country'] == 'CT') {
    $format['locality_block']['administrative_area'] = array(
      '#title' => t('State'),
      '#size' => 10,
      '#required' => TRUE,
      '#prefix' => ' ',
      '#attributes' => array('class' => array('state')),
    );

    if (isset($format['locality_block']['administrative_area'])) {
      $state_options = array(
        ''   => t('--'),
        'B'  => t('Barcelona'),
        'GI' => t('Girona'),
        'L'  => t('Lleida'),
        'T'  => t('Tarragona'),
      );

      if (!empty($format['locality_block']['administrative_area']['#options'])) {
        $state_options += $format['locality_block']['administrative_area']['#options'];
      }

      $format['locality_block']['administrative_area']['#title'] = t('Province');
      $format['locality_block']['administrative_area']['#options'] = $state_options;
    }
  }
}
